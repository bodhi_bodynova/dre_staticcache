<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id' => 'dre_staticcache',
    'title' => [
        'en' => '<img src="../modules/bender/dre_staticcache/out/img/favicon.ico" title="Bodynova Service Modul">odynova Cache Modul',
        'de' => '<img src="../modules/bender/dre_staticcache/out/img/favicon.ico" title="Bodynova Service Modul">odynova Cache',
    ],
    'description' => [
        'en' => 'cache scripts',
        'de' => 'Cache Modul',
    ],
    'thumbnail' => 'out/img/logo_bodynova.png',
    'version' => '0.0.1',
    'author' => 'Bodynova GmbH',
    'url' => 'https://bodynova.de',
    'email' => 'support@bodynova.de',
    'controllers' => [
    ],
    'extend' => [
        \OxidEsales\Eshop\Core\ShopControl::class => \Bender\dre_StaticCache\Core\ShopControl::class
    ],
    'templates' => [

    ],
    'blocks' => [
    ],
    'settings' => [
        [
            'group' => 'main',
            'name' => 'iCacheLifetime',
            'type' => 'str',
            'value' => '28800'
        ],
        [
            'group' => 'main',
            'name' => 'aCachedClasses',
            'type' => 'arr',
            'value' => ['OxidEsales\Eshop\Application\Controller\StartController',
                'OxidEsales\Eshop\Application\Controller\ArticleListController',
                'OxidEsales\Eshop\Application\Controller\ArticleDetailsController',
                'OxidEsales\Eshop\Application\Controller\ContentController'],
        ],
        [
            'group' => 'main',
            'name' => 'bHtmlMinify',
            'type' => 'bool',
            'value' => 'true'
        ],
    ],
];