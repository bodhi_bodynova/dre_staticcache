<?php

namespace Bender\dre_StaticCache\src;

use \OxidEsales\Eshop\Core\Registry;

class registry_cache_container
{
    /**
     * Magic getter to  return cached values.
     *
     * @param $name string
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }

        return null;
    }

    /**
     * Magic setter to save stuff.
     *
     * @param $name string
     * @param $value string
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * @param $sVarName
     * @param null $sShopId
     * @param string $sModule
     * @return mixed|object
     */
    public function getShopConfVar($sVarName, $sShopId = null, $sModule = '')
    {
        $name = $sVarName . $sShopId . $sModule;

        if (isset($this->$name)) {
            return $this->$name;
        }

        $res = Registry::getConfig()->getShopConfVar($sVarName, $sShopId, $sModule);

        $this->$name = $res;

        return $res;
    }
}
