<?php

namespace Bender\dre_StaticCache\src;

use \OxidEsales\Eshop\Core\Registry;
use \OxidEsales\Eshop\Core\Module\Module;
use \OxidEsales\Eshop\Application\Model\User;

class base_html_cache
{

    //ToDo: make widgets configurable separate to skip some of them with full basket.
    protected $_aCachableControllers = [];

    protected $_cacheBackend = null;

    public function __construct($oCacheBackend)
    {
        $this->_cacheBackend         = $oCacheBackend;
        $this->_aCachableControllers = $this->getCachableControllers();
    }

    /**
     * checks for a valid cache and if found, outputs it and skips the other rendering
     */
    public function getCacheContent()
    {
        if (!$this->isCachableRequest()) {
            return false;
        }

        $key = $this->getCacheKey();

        $sContent = $this->_cacheBackend->getCache($key);

        if (is_string($sContent)) {
            return $sContent;
        }

        return false;
    }

    /**
     * Minify the html output
     *
     * ToDo: Make configurable -> DONE!
     *
     * @param $sValue string
     * @return string
     */
    protected function _minifyHtml($sValue)
    {
        $aSearch = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/', # Delete multispace (Without \n)
            '/\>\s+\</', # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'); # strip whitespaces between = "'

        $aReplace = array(
            "\n",
            "\n",
            " ",
            "",
            " ",
            "><",
            "$1>",
            "=$1");
        #$aSearch   = ['/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+\/s'];
        #$aReplace  = [' ', ''];
        $sMinified = preg_replace($aSearch, $aReplace, $sValue);

        return $sMinified;
    }

    /**
     * Adds the default OXID version Tags.
     * Please keep this function with respect for OXID!
     *
     * @param $sOutput
     * @return full content string
     */
    public function addCacheVersionTags($sOutput)
    {
        $oConf = Registry::getConfig();
        // DISPLAY IT
        $sVersion  = $oConf->getVersion();
        $sEdition  = $oConf->getFullEdition();
        $sCurYear  = date("Y");
        $sShopMode = "";

        // SHOW ONLY MAJOR VERSION NUMBER
        $aVersion      = explode('.', $sVersion);
        $sMajorVersion = reset($aVersion);

        // Replacing only once per page
        $sOutput = str_ireplace("</head>", "</head>\n  <!-- CACHED ".date('Y-m-d H:i:s')."-->", ltrim($sOutput));

        return $sOutput;
    }

    /**
     * Create the Cache
     *
     * @param $sContent fully rendered content
     */
    public function createCache($sContent)
    {
        if (!$this->isCachableRequest()) {
            return;
        }

        if (Registry::getConfig()->getConfigParam('bHtmlMinify')) {
            $sContent = $this->_minifyHtml($sContent);
            $sContent = $this->addCacheVersionTags($sContent);
        }else{
            $sContent = $this->addCacheVersionTags($sContent);
        }

        if (false == Registry::getConfig()->isUtf()) {
            $sCharset = Registry::getLang()->translateString('charset');
            $sContent = mb_convert_encoding($sContent, 'UTF-8', $sCharset);
        }

        if (!Registry::getConfig()->isProductiveMode()) {
            $sContent = str_ireplace("</head>", "</head>\n  <!-- CACHED -->", ltrim($sContent));//str_ireplace("- http://www.oxid-esales.com -->", "- http://www.oxid-esales.com (from Cache) -->", ltrim($sContent));
        }

        $key = $this->getCacheKey();
        $this->_cacheBackend->setCache($key, $sContent);
    }

    /**
     * Check if this request could be cached.
     *
     * @return bool
     */
    public function isCachableRequest()
    {
        if (!in_array($this->getClassName(), $this->_aCachableControllers)) {
            return false;
        }

        //Check if any filter from makaira module has been set!
        if ($this->sClassName == 'OxidEsales\Eshop\Application\Controller\ArticleListController') {
            //OXID standard session filter
            $aSessionFilter = Registry::getSession()->getVariable('session_attrfilter');
            $sActCat        = Registry::getConfig()->getRequestParameter('cnid');
            $SessfilActCat  = $aSessionFilter[$sActCat];
            $aAttrFilter    = (is_array($SessfilActCat)) ? array_filter($SessfilActCat) : 0;
            if (!empty($aAttrFilter)) {
                return false;
            }
            //Makaira filter
            //we need to use method_exists as there is a bug in oxmodule::isActive() prior OXID 4.7.11
            if (method_exists(Registry::get('oxViewConfig'), 'getAggregationFilter')) {
                $oxModule = oxNew(Module::class);
                $oxModule->load('makaira/connect');
                if ($oxModule->isActive()) {
                    $aFilter = array_filter(Registry::get('oxViewConfig')->getAggregationFilter());
                    if (!empty($aFilter)) {
                        return false;
                    }
                }
            }
        }
        //END Filter check

        if ($this->sFunction) {
            return false;
        }
        $oUser = oxNew(User::class);
        if ($oUser->loadActiveUser() !== false) {
            return false;
        }
        $oConf   = Registry::getConfig();
        $partial = $oConf->getRequestParameter('renderPartial');
        if (!empty($partial)) {
            return false;
        }
        if ($this->_hasBasketItems()) {
            return false;
        }

        return true;
    }

    public function getClassName()
    {
        if (isset($this->sClassName)) {
            return $this->sClassName;
        }
        $oConf            = Registry::getConfig();
        $oActView         = $oConf->getActiveView();
        $sClassName       = $oActView->getClassName();
        $this->sClassName = $sClassName;

        return $this->sClassName;
    }

    /**
     * I guess we should refactor this.
     * Anway the extending classes inject data via this. :-/
     *
     * @deprecated
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Calclulate the Cache Key
     *
     * @return string cache Key
     */
    public function getCacheKey()
    {
        //ToDo: Change this for caching widgets
        $oUtilsServer = Registry::get("oxUtilsServer");
        $requestUrl   = $oUtilsServer->getServerVar("REQUEST_URI");

        //Parameter entfernen, da Datei aufgr. Parameter (?rand=1) mehrfach angelegt wurde
        $requestUrl = str_replace('?rand=1', '', $requestUrl);

        //Für Ecs B2B Modul, damit Shop je nach Kunde mit oder ohne Preise gezeigt wird.
        $oxModule = oxNew(Module::class);
        $oxModule->load('ecs_easyb2b');
        if ($oxModule->isActive()) {
            if ($b2bvar = $this->_isEcsB2BSessionRunning()) {
                $requestUrl = $requestUrl . $b2bvar;
            }
        }

        //Für Ecs Cookie Banner (Tracking Modul).
        if ($trackingCookie = Registry::getUtilsServer()->getOxCookie('consent')) {
            $requestUrl = $requestUrl . 'cc_' . $trackingCookie;
        }

        $ClassName = str_replace('OxidEsales\Eshop\Application\Controller\\', '', $this->getClassName());
        $key       = $ClassName . '_' . md5($requestUrl);
        return $key;
    }

    /**
     * Check if there are items in the basket which will lead to a non cachable request.
     * //ToDo: this could be skipped for caching most of the widgets. right?!
     *
     * @return bool
     */
    protected function _hasBasketItems()
    {
        $oBasket = Registry::getSession()->getBasket();
        if ($oBasket && $oBasket->getProductsCount() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Returns cachable controllers according to backend settings
     *
     * @return array
     */
    protected function getCachableControllers()
    {
        return Registry::getConfig()->getShopConfVar('aCachedClasses', null, 'module:dre_staticcache');
    }

    /**
     * Für Ecs B2B Modul, damit Shop je nach Kunde mit oder ohne Preise gezeigt wird.
     */
    protected function _isEcsB2BSessionRunning()
    {
        $oSession = Registry::getSession();
        if ($oSession and $oSession->hasVariable('b2bsession')) {
            if ($var = $oSession->getVariable('b2bsession')) {
                return $var;
            }
        }
        $b2bcookie = Registry::getUtilsServer()->getOxCookie('b2b');
        if ($cookie = $b2bcookie) {
            return $cookie;
        }
        return false;
    }
}
