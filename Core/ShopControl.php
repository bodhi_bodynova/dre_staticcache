<?php

namespace Bender\dre_StaticCache\Core;


use \OxidEsales\Eshop\Core\Registry;
use \Bender\dre_StaticCache\src\base_html_cache;
use \Bender\dre_StaticCache\src\backends\file_backend;

class ShopControl extends ShopControl_parent
{

    protected function _process($sClass, $sFunction, $aParams = null, $aViewsChain = null)
    {
        if (!isset($this->oCache)) {
            //ToDo: Read from configuration, when there is more then one backend available
            //$cacheBackendType = 'file_backend';
            $cacheBackend = Registry::get(file_backend::class);
            $this->oCache = oxNew(base_html_cache::class, $cacheBackend);
        }
        $this->oCache->sClassName = $sClass;
        $this->oCache->sFunction = $sFunction;

        parent::_process($sClass, $sFunction, $aParams, $aViewsChain);
    }

    protected function _render($oViewObject)
    {
        if (!isAdmin() && isset($this->oCache)) {
            $oCache = $this->oCache;
            $sCachedContent = $oCache->getCacheContent();

            if ($sCachedContent !== false) {
                return $sCachedContent;
            }
        }

        $sContent = parent::_render($oViewObject);

        if (!isAdmin() && isset($this->oCache)) {
            $this->oCache->createCache($sContent);
        }

        return $sContent;
    }
}

